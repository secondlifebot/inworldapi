var express = require('express');
const request = require('request');
const {parseRequest, log, handleRequest} = require("../util");

var router = express.Router();


const API_ENDPOINT = 'http://127.0.0.1:3000/message'; // replace with your API endpoint

async function onRequest(req, res, next) {
  console.error('Hello');
  const new_auth = req.headers.authorization ? req.headers.authorization : 'X X:Y';
  const options = {
    url: API_ENDPOINT,
    qs: {
      // add any query parameters here
      m: req.query.m,
      scene: req.query.scene,
      sessionId: req.query.sessionId,
      auth: new_auth.split(' ')[1]
    }
  };
  
  request(options, (error, response, body) => {
    if (error) {
      console.error(error);
      return res.status(500).send('Error');
    }
    const jsonString = body;

    const jsonStringArray = jsonString.split(/}\s*{/);
  
    let extractedData = [];
    let mysess = 0;
    jsonStringArray.forEach((jsonStr, index) => {

      // Add back the missing `}` for all parts except the last one
      let jsonStringToParse = jsonStr;
      if (index < jsonStringArray.length - 1) {
        jsonStringToParse = jsonStringToParse + '}';
      } 
      if (index > 0) {
        jsonStringToParse = '{' + jsonStringToParse;
      } 
      try {
        console.error(jsonStringToParse);
        console.error(jsonStringArray.length + '=========' + index);
        const parsedData = JSON.parse(jsonStringToParse);
        // Loop through a list in the parsed JSON data
    //parsedData.list.forEach(item => {
          // Loop through a list in the JSON data
          const fieldValue1 = parsedData.text;
          mysess = parsedData.sessionId;
          // Add the extracted fields to the extractedData array
          extractedData.push({ field1: fieldValue1 });
      } catch (error) {
        console.error(error);
        res.status(500).send('Error');
      }
    });
    extractedData.push({ "sessionId": mysess });
    // Send the extracted data as JSON response
    res.json(extractedData);
});
}

/* GET home page. */
router.get('/', onRequest);

module.exports = router;
